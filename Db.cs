using neocomplexx.Models;
using System.Collections.Generic;

namespace neocomplexx {
	public class Db {
		public List<Cliente> Clientes { get; }

		public Db() {
			Clientes = new List<Cliente>();

			for (int i = 1; i <= 10; i++) {
				Clientes.Add(new Cliente("Nombre " + i.ToString(), "Apellido " + i.ToString(), "Direccion " + i.ToString()));
			}
		}

		public void AgregarCliente(Cliente cliente) {
			Clientes.Add(cliente);
		}

		public void BorrarCliente(int id) {
			Clientes.RemoveAt(id - 1);
		}

		public void ModificarCliente(int id, Cliente cliente) {
			Clientes[id - 1] = cliente;
		}

		public Cliente ObtenerCliente(int id) {
			return Clientes[id - 1];
		}
	}
}