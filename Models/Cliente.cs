using System;

namespace neocomplexx.Models {
	public class Cliente {
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		public string Direccion { get; set; }

		public Cliente(string nombre, string apellido, string direccion) {
			Nombre = nombre;
			Apellido = apellido;
			Direccion = direccion;
		}
	}
}