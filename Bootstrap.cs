using System;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Nancy.Configuration;
using neocomplexx;

public class CustomNancyBootstrapper : DefaultNancyBootstrapper {
	private DateTime cronometroInicio;
	private DateTime cronometroFin;

	protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines) {
        base.ApplicationStartup(container, pipelines);

		pipelines.BeforeRequest.AddItemToEndOfPipeline((ctx) => {
    		cronometroInicio = DateTime.Now;

			return null;
		});

        //CORS Enable
        pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) => {
            ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                .WithHeader("Access-Control-Allow-Methods", "POST,GET,DELETE,PUT,UPDATE")
                .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");
			
			cronometroFin = DateTime.Now;
			TimeSpan cronometro = cronometroFin.Subtract(cronometroInicio);
			Console.WriteLine("Tiempo transcurrido en milisegundos: " + cronometro.TotalMilliseconds);
        });
	}

	public override void Configure(INancyEnvironment environment) {
        environment.Tracing(enabled: false, displayErrorTraces: true);
        base.Configure(environment);
    }

	protected override void ConfigureApplicationContainer(TinyIoCContainer container) {
        container.Register<Db>(new Db());
    }
}