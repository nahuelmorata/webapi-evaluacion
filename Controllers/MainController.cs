﻿using System;
using System.Collections.Generic;
using System.IO;
using Nancy;
using neocomplexx.Models;
using Newtonsoft.Json;

namespace neocomplexx.Controllers {
    public class Clientes : Nancy.NancyModule {
		public Clientes(Db db) {
			Get("/clientes", _ => {
				return GetClientes(db);
			});
			Get("/clientes/{id}", args => {
				return GetCliente(args, db);
			});
			Post("/clientes/{id}", args => {
				return PostCliente(args, db);
			});
			Put("/clientes", _ => {
				return PutCliente(db);
			});
			Delete("/clientes/{id}", args => {
				return DeleteCliente(args, db);
			});
		}
		public Response GetClientes(Db db) {			
			var response = (Response) JsonConvert.SerializeObject(db.Clientes);
			response.ContentType = "application/json";

			return response;
		}

		public Response GetCliente(DynamicDictionary args, Db db) {
			dynamic id = "";
			args.TryGetValue("id", out id);
			
			var response = (Response) JsonConvert.SerializeObject(db.ObtenerCliente(Int32.Parse(id)));
			response.ContentType = "application/json";

			return response;
		}

		public Response PostCliente(DynamicDictionary args, Db db) {
			StreamReader reader = new StreamReader(Request.Body);
			string bodyText = reader.ReadToEnd();

			dynamic data = JsonConvert.DeserializeObject(bodyText);

			dynamic id = "";	
			dynamic nombre = data.nombre;
			dynamic apellido = data.apellido;
			dynamic direccion = data.direccion;

			args.TryGetValue("id", out id);

			Cliente clienteModificar = db.ObtenerCliente(Int32.Parse(id));

			clienteModificar.Nombre = nombre.ToString();
			clienteModificar.Apellido = apellido.ToString();
			clienteModificar.Direccion = direccion.ToString();

			db.ModificarCliente(Int32.Parse(id), clienteModificar);

			var response = (Response) "{}";
			response.ContentType = "application/json";

			return response;
		}

		public Response PutCliente(Db db) {
			StreamReader reader = new StreamReader(Request.Body);
			string bodyText = reader.ReadToEnd();

			dynamic data = JsonConvert.DeserializeObject(bodyText);
	
			dynamic nombre = data.nombre;
			dynamic apellido = data.apellido;
			dynamic direccion = data.direccion;

			Cliente clienteNuevo = new Cliente(nombre.ToString(), apellido.ToString(), direccion.ToString());

			db.AgregarCliente(clienteNuevo);

			var response = (Response) "{}";
			response.ContentType = "application/json";

			return response;
		}

		public Response DeleteCliente(DynamicDictionary args, Db db) {
			dynamic id = "";

			args.TryGetValue("id", out id);

			db.BorrarCliente(Int32.Parse(id));

			var response = (Response) "{}";
			response.ContentType = "application/json";

			return response;
		}
	}
}
